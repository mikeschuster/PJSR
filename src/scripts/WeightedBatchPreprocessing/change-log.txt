===============================================================================
Weighted Batch Preprocessing Script Changelog
===============================================================================

2020/08/16 - v1.4.8

- FWHM, eccentricity and SNR are cached into the measured images's FITS header

- when computing the image measurememts, cached values from FITS header are used 
  if present unless one or more are missing

- a new option "Force image measurements" added to force WBPP to measure the images
  disregarding the cached values eventlayy present in FITS header

-------------------------------------------------------------------------------
2020/08/03 - v1.4.7

- Image Registration: new "Apply" checkbox added to make this step optional.

- Subframe Weighting: checkbox "Compute weights after registration" removed.

- Best frame auto-detection: checkbox moved under the Registration Reference
  Image panel.

- Smart Report: typo fixed.

- Do not show image windows generated by ImageIntegration (new 'showImages'
  parameter available since core version 1.8.8-6).

- PixInsight core version >= 1.8.8-6 required.

-------------------------------------------------------------------------------
2020/05/12 - v1.4.6

- Optimization: Light frames are not measured if "calibrate only" is checked.

- Large-scale pixel rejection high and low added to light's Integration panel.

- Bugfix: "calibrated darks" flag not properly restored.

- Bugfix: issue when no dark frames are provided. Original forum report:
    https://pixinsight.com/forum/index.php?threads/
    staralignment-referenceimage-invalid-argument-type-string-expected.14223/

- Bugfix: filter name not assigned when custom files were added.

- Bugfix: first subgroup of shortest exposure were processed if no calibration
          files were provided for light or flats group.

- Logging improved.

- Code refactoring.

-------------------------------------------------------------------------------
2020/05/07 - v1.4.5

- Bugfix: Assignment to undeclared variable 'images'. Original bug report:
   https://pixinsight.com/forum/index.php?threads/
   weightedbatchpreprocessing-programming-warning-on-undeclared-variable.14085/

- Improve usage of the following ImageIntegration parameters:
   * subtractPedestals = false
   * truncateOnOutOfRange = true only for integration of light frames
   * winsorizationCutoff = 5 sigma units
   * esdLowRelaxation = 1.5
   * autoMemorySize = true
   * autoMemoryLimit = 0.75
  Some of these parameters could be made user-configurable in a future version.

- Support the following values of the IMAGETYP FITS keyword (case insensitive):
   * 'FLATDARK'
   * 'FLAT DARK'
   * 'DARKFLAT'
   * 'DARK FLAT'
   All of these keyword values are now supported to identify dark frames.
   Original forum wish list thread:
   https://pixinsight.com/forum/index.php?threads/
   understand-darkflat-image-type-in-wbpp-bpp.14545/

- Prefer the 'Hack' font to 'DejaVu Sans Mono' for user interface elements
  representing monospaced text. This is more coherent with recent versions of
  PixInsight.

-------------------------------------------------------------------------------
2020/01/23 - v1.4.4

- The "Calibrate master darks" checkbox is now only enabled, and can only be
  unchecked, if the "Use master dark" option is enabled. This update removes
  potential problems and confusion regarding the meaning of disabling master
  dark calibration when single darks are provided.

-------------------------------------------------------------------------------
2020/01/20 - v1.4.3

- New StackEngine.masterDarkIncludesBias property. Contributed by Armando
  Beneduce. This new parameter allows using pre-calibrated master dark frames,
  i.e. bias-subtracted master darks. This adds flexibility to the WBPP script,
  but deviates from our recommended best practices. Appropriate diagnostics and
  warning messages are shown when this property is set to false.

- Bugfix: Fixed broken acquisition of exposure times from FITS ketywords.
  Contributed by Armando Beneduce.

- Bugfix: Don't generate diagnostics errors for light file lists smaller than
  three frames when StackEngine.calibrateOnly is true and/or
  StackEngine.integrate is false.

- Bugfix: Non-function-call use of StackEngine.exposuresToString in
  StackEngine.doLight().

- Improved numeric error checks in image weights computation routines.

-------------------------------------------------------------------------------
2019/11/24 - v1.4.2

- Corrected some typos.

- Bugfix: Fixed broken StackEngine.exportParameters() method.

- Bugfix: masterFlat items are now assigned correctly to each group.

- Code refactoring: Extensions to core objects isolated into a Utility object.

- Code refactoring: Replaced all global scope variable declarations ('var')
  with local scope ('let') when adequate.

- Improvement: The StackEngine global variable is garbage-collected at the end
  of script execution.

- "Generate Weights" flat is now decoupled from "Use best frame as reference
  for registration" flag.

-------------------------------------------------------------------------------
2019/11/19 - v1.4.1

- First official release.

- Removed the (now useless) exportCalibrationFiles script parameter.

- Bugfix: Fixed StackEngine frame group export/import routines.

-------------------------------------------------------------------------------
2019/11/19 - v1.4.0

- Light exposure tolerance flag replaces the "Group lights with different
  exposure".

- Reference frame selected amog the files with the lowest binning.

- Bugfix: Drizzle files not update by Image Integration process.

-------------------------------------------------------------------------------
v1.3.5

- Automatic light frame weights computation.

- Automatic selection of the best reference frame for registration.

- Multiple duration/filter/binning light frame groups processed.

- "Group lights with different exposures" flag added to optionally group all
  light frames despite their duration.

- Light frames are grouped by duration during calibration process to match the
  correct dark frame (useful when light frames with different exposure are
  grouped together).

- "auto" rejection option to select the best rejection strategy during
  integration.

- Smart naming: BINNING, FILTER and EXPTIME can be added to file names or
  including folder to provide these values when no proper keywords are stored
  into frame files.

- Flat frames are always calibrated with master bias if a matching master bias
  is provided.

- Smart reporting: summary of operations shown at the end of the process.

- Advanced diagnostic.

- "Save frame groups on exit" flag added.
